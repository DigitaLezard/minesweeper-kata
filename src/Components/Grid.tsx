import React from 'react';
import { GameContext } from '../GameContext';
import { Cell } from './Cell';
import { Game } from './Game';
import cloneDeep from 'lodash/cloneDeepWith';

export const Grid: React.FunctionComponent = () => {
    let { grid, previousGrid, score, updateGridCellStatus } = React.useContext(GameContext);
    const [scoreValue, setScore] = React.useState(score.value);

    setInterval(() => {
        setScore(score.value);
    }, 2000);

    const handleClick = (index: number, button: number) => {
        if (!grid.isVictorious() && !grid.isDefeated()) {
            Object.assign(previousGrid.cells, cloneDeep(grid.cells));
            updateGridCellStatus(index, button === 0 ? 'dig' : 'flag');
            (button === 0) ? grid.dugIfNoBombsAround(index) : handleScore(1);
            (grid.isVictorious() || grid.isDefeated()) ? score.destroyTimer() : undefined;
        }
    };

    const undo = () => {
        if (hasUndoState()) {
            Object.assign(grid.cells, cloneDeep(previousGrid.cells));

            grid.cells.forEach((cell, index) => {
                updateGridCellStatus(index, cell.dug === false ? 'untouch' : 'dig');
            })

            handleScore(5);

            if (!score.hasTimer()) {
                score.setTimer();
            }
        }
    };

    const hasUndoState = () => {
        return (JSON.stringify(grid.cells) !== JSON.stringify(previousGrid.cells));
    };

    const handleScore = (value: number) => {
        if (score.value > 0 && score.value - value >= 0) {
            score.value = score.value - value;
        } else if (score.value > 0 && score.value - value < 0) {
            score.value = 0;
        }
        setScore(score.value);
    }

    const gameOver =
        (grid.isDefeated() && 'defeat') ||
        (grid.isVictorious() && 'victory') ||
        false;

    return (
        <React.Fragment>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Game gameOver={gameOver} score={scoreValue} />
                <div
                    style={{
                        display: 'flex',
                        border: '1px solid black',
                        boxSizing: 'content-box',
                        margin: 'auto',
                        marginTop: '1%',
                        flexWrap: 'wrap',
                        width: `calc(40px * ${grid.column})`,
                    }}
                >

                    {grid.map((cell, index) => (
                        <Cell
                            key={index}
                            status={cell.status}
                            haveBombAround={cell.hasBombAround}
                            numberOfBombAround={cell.numberOfBombsAround}
                            onclick={(ev: MouseEvent) =>
                                handleClick(index, ev.button)
                            }
                        />
                    ))}
                </div>
                <button
                    disabled={!hasUndoState()}
                    onClick={undo}
                    style={{
                        width: '70px',
                        marginTop: '2%',
                        cursor: 'pointer'
                    }}>
                    Undo
                </button>
            </div>
        </React.Fragment>
    );
};
