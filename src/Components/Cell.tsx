import React from 'react';
import { CellStatus } from '../Domain/Cell';

type CellProps = {
    haveBombAround: Boolean;
    numberOfBombAround?: Number;
    status: CellStatus;
    onclick: Function;
};

const emojis = (status: CellStatus, nbrOfBombAround: any) => {
    switch (status) {
        case 'untouched':
            return '';
        case 'flagged':
            return '🚩';
        case 'detonated':
            return '💥';
        case 'dug':
            return dugStyle(nbrOfBombAround);
    }
};

const dugStyle = (nbrOfBombAround: any) => {
    return (nbrOfBombAround > 0) ? '🔴' : '';
}

const cellStyle = (status: CellStatus): React.CSSProperties => ({
    width: '40px',
    height: '40px',
    textAlign: 'center',
    lineHeight: '40px',
    border: '1px solid black',
    boxSizing: 'border-box',
    cursor: 'pointer',
    boxShadow: status === 'untouched' || status === 'flagged'
        ? `inset -4px -5px 3px -2px rgba(0,0,0,0.56), 
        inset 5px 4px 3px -2px rgba(245,237,245,1)`
        : `inset -4px -5px 3px -2px rgba(102,102,102,1), 
        inset 5px 4px 3px -2px rgba(38,38,38,1)`,
    backgroundColor:
        status === 'untouched' || status === 'flagged' ? '#ccc' : '#808080',
});

const BombArround = (props: CellProps) => {
    return (props.status === 'dug' && props.haveBombAround)
        ? <span>{props.numberOfBombAround}</span>
        : null;
}

export const Cell: React.FunctionComponent<CellProps> = props => {
    return (
        <div
            onClick={ev => {
                ev.preventDefault();
                props.onclick(ev);
            }}
            onContextMenu={ev => {
                ev.preventDefault();
                props.onclick(ev);
            }}
            style={cellStyle(props.status)}
        >
            <BombArround {...props} />
            {emojis(props.status, props.numberOfBombAround)}
        </div>
    );
};

