import React from 'react';

type GameProps = {
    gameOver: false | 'victory' | 'defeat';
    score: number;
};

export const Game: React.FunctionComponent<GameProps> = props => {
    return <div
        style={{
            flexDirection: 'row',
            alignItems: 'center',
            width: '15%',
            height: '10rem',
            textAlign: 'center',
        }}>
        <h5 style={{
            justifySelf: 'flex-end',
            fontSize: '25px'
        }}>Score: {props.score}</h5>

        <h2 style={{
            fontSize: '50px'
        }}>{props.gameOver}</h2>

    </div>;
};