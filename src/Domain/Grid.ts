import { Cell, CellAction } from './Cell';

export type Cells = Array<Cell>;

export class Grid {
    [key: number]: number;
    private _column: number;
    private _cells: Cells;

    static generate(row: number, column: number, minesCount: number): Grid {
        const length = row * column;
        let cells: Cells = [];
        for (let i = 0; i < length; i++) {
            const cell = minesCount > i ? Cell.withBomb() : Cell.withoutBomb();
            cells.push(cell);
        }

        let index = -1;
        while (++index < length) {
            const rand = index + Math.floor(Math.random() * (length - index));
            const cell = cells[rand];

            cells[rand] = cells[index];
            cells[index] = cell;
        }

        return new Grid(column, cells);
    }

    constructor(column: number, cells: Cells) {
        if (!Number.isInteger(column)) {
            throw new TypeError('column count must be an integer');
        }

        if (cells.length % column !== 0 || cells.length === 0) {
            throw new RangeError(
                'cell count must be dividable by column count'
            );
        }

        this._column = column;
        this._cells = cells;
        let context = this;
        this._cells.forEach((val: Cell, index: any) => {
            val.numberOfBombsAround = context.checkBombAround(index);
            val.hasBombAround = (context.checkBombAround(index) > 0);
        });
    }

    [Symbol.iterator]() {
        return this._cells[Symbol.iterator]();
    }

    map(
        callbackfn: (value: Cell, index: number, array: Cell[]) => {},
        thisArg?: any
    ) {
        return this._cells.map(callbackfn);
    }

    cellByIndex(index: number): Cell | undefined {
        return this._cells[index];
    }

    cellByCoodinates(x: number, y: number): Cell | undefined {
        return this._cells[this._column * y + x];
    }

    sendActionToCell(cellIndex: number, action: CellAction): Grid {
        const cells = [...this._cells];
        const cell = cells[cellIndex];

        cells[cellIndex] = cell[action]();
        return new Grid(this._column, cells);
    }

    isDefeated = () => {
        for (let cell of this) {
            if (cell.detonated === true) return true;
        }
        return false;
    };

    isVictorious = () => {
        let unduggedCell = 0;
        for (let cell of this) {
            if (
                (cell.dug === false && cell.flagged === false && cell.hasBomb === false) ||
                cell.detonated === true
            ) {
                return false;
            }
        }
        return true;
    };

    get column() {
        return this._column;
    }

    get cells() {
        return this._cells;
    }

    checkIfHasBomb(cellIndex: number) {
        return (this._cells[cellIndex].hasBomb) ? 1 : 0;
    }

    isOnTopBorder(cellIndex: number): boolean {
        return cellIndex < this._column
    }

    IsOnBottomBorder(cellIndex: number): boolean {
        return cellIndex > (this._cells.length - 1) - this._column
    }

    isOnLeftBorder(cellIndex: number): boolean {
        return !(cellIndex % this._column > 0)
    }

    isOnRightBorder(cellIndex: number): boolean {
        return !(cellIndex % this._column < this._column - 1)
    }

    dugIfNoBombsAround(cellIndex: number) {
        if (this._cells[cellIndex].numberOfBombsAround === 0 && !this._cells[cellIndex].hasBomb) {
            this.actionAround(cellIndex, (index: number) => {
                if (this._cells[index].status != 'dug'
                    && this._cells[index].status != 'detonated') {
                    this._cells[index].dig();
                    this.dugIfNoBombsAround(index);
                }
            });
        }
    }

    checkBombAround(cellIndex: number): number {
        let numberOfBombAround = 0;
        this.actionAround(cellIndex, (index: number): void => {
            numberOfBombAround += this.checkIfHasBomb(index);
        });
        return numberOfBombAround;
    }

    actionAround(cellIndex: number, cb: Function) {
        if (!this.isOnTopBorder(cellIndex)) {
            cb(cellIndex - this._column);
            (!this.isOnLeftBorder(cellIndex)) ? cb((cellIndex - this._column) - 1) : undefined;
            (!this.isOnRightBorder(cellIndex)) ? cb((cellIndex - this._column) + 1) : undefined;
        }

        if (!this.IsOnBottomBorder(cellIndex)) {
            cb(cellIndex + this._column);
            (!this.isOnLeftBorder(cellIndex)) ? cb((cellIndex + this._column) - 1) : undefined;
            (!this.isOnRightBorder(cellIndex)) ? cb((cellIndex + this._column) + 1) : undefined;
        }

        if (!this.isOnLeftBorder(cellIndex)) {
            cb(cellIndex - 1);
        }

        if (!this.isOnRightBorder(cellIndex)) {
            cb(cellIndex + 1);
        }
    }
}
