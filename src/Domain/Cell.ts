export type CellStatus = 'untouched' | 'flagged' | 'dug' | 'detonated';
export type CellAction = 'dig' | 'flag' | 'untouch';

export class Cell {
    private _bomb: boolean;
    private _hasBombsAround: Boolean;
    private _numberOfBombsAround: number;
    private _flagged: boolean;
    private _dug: boolean;

    static withBomb(): Cell {
        return new Cell(true, false, false);
    }

    static withoutBomb(): Cell {
        return new Cell(false, false, false);
    }

    constructor(withBomb: boolean, flagged: boolean, dug: boolean) {
        this._bomb = withBomb;
        this._flagged = flagged;
        this._dug = dug;
        this._hasBombsAround = false;
        this._numberOfBombsAround = 0;
    }

    flag(): Cell {
        if (this._dug === true) {
            throw new Error('This cell has already been dug');
        }
        return new Cell(this._bomb, !this._flagged, this._dug);
    }

    dig(): Cell {
        this.dug = true;
        return this;
    }

    untouch(): Cell {
        this.dug = false;
        return this;
    }

    get detonated(): boolean {
        return this._bomb && this.dug;
    }

    get hasBomb(): boolean {
        return this._bomb;
    }

    get hasBombAround() {
        return this._hasBombsAround;
    }

    get numberOfBombsAround(): number {
        return this._numberOfBombsAround;
    }

    get flagged(): boolean {
        return this._flagged;
    }

    get dug(): boolean {
        return this._dug;
    }

    set dug(value: boolean) {
        this._dug = value;
    }

    get status(): CellStatus {
        if (this.detonated) {
            return 'detonated';
        }
        if (this.dug) {
            return 'dug';
        }
        if (this.flagged) {
            return 'flagged';
        }
        return 'untouched';
    }

    set hasBombAround(value: Boolean) {
        this._hasBombsAround = value;
    }

    set numberOfBombsAround(value: number) {
        this._numberOfBombsAround = value;
    }
}