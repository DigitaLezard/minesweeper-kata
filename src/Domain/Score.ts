
export class Score {
    private _value: number;
    private _timer: any;

    constructor(scoreValue: number) {
        this._value = scoreValue;
        this.setTimer();
    }

    get value(): number {
        return this._value
    }

    set value(value: number) {
        this._value = value
    }

    public setTimer() {
        this._timer = setInterval(() => {
            if (this._value > 0) {
                this._value -= 1;
            }
        }, 5000);
    }

    public destroyTimer() {
        clearInterval(this._timer);
        this._timer = undefined;
    }

    public hasTimer() {
        return (!!this._timer)
    }
}