import React from 'react';
import { CellAction } from './Domain/Cell';
import { Grid } from './Domain/Grid';
import cloneDeep from 'lodash/cloneDeep';
import { Score } from './Domain/Score';

type GameContextProps = {
    grid: Grid;
    previousGrid: Grid;
    score: Score;
    updateGridCellStatus: (index: number, status: CellAction) => void;
};

type GridCustomHook = [Grid, Grid, Score, (index: number, action: CellAction) => void];

const height = 10;
const width = 10;
const bomb = 10;

const initialContext: GameContextProps = {
    grid: Grid.generate(height, width, bomb),
    previousGrid: Grid.generate(height, width, 0),
    score: new Score(height * width),
    updateGridCellStatus: () => { },
};

Object.assign(initialContext.previousGrid.cells, cloneDeep(initialContext.grid.cells));

const useStateGridCells = (initialValue: { grid: Grid; previous: Grid; score: Score; }): GridCustomHook => {
    const [{ grid, previous, score }, setGrid] = React.useState(initialValue);

    return [
        grid,
        previous,
        score,
        (index: number, action: CellAction) => {
            const newGrid = grid.sendActionToCell(index, action);
            setGrid({ grid: newGrid, previous: previous, score: score });
        }
    ];
};

export const GameContext = React.createContext<GameContextProps>(
    initialContext
);

export const GameContextProvider: React.FunctionComponent<
    React.ReactNode
> = props => {
    const [grid, previousGrid, score, updateGridCellStatus] = useStateGridCells({ grid: initialContext.grid, previous: initialContext.previousGrid, score: initialContext.score });

    return (
        <GameContext.Provider value={{ grid, previousGrid, score, updateGridCellStatus }}>
            {props.children}
        </GameContext.Provider>
    );
};